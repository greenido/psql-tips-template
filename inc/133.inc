    <div class="tip">The <code>\gset</code>
      metacommand will execute the result of the current query or the last query
      if the current query buffer is empty and assign variables named as the
      column names with the value. It only works if the query returns only one
      row.
      <pre><code class="hljs bash">laetitia=# select * 
laetitia-# from test 
laetitia-# where id=5;
 id | value 
----+-------
  5 | bla
(1 row)

laetitia=# \gset
laetitia=# \echo 'id: ' :id ', value: ' :value
id:  5 , value:  bla</code></pre>This feature is available since Postgres 9.3.
	</div>
